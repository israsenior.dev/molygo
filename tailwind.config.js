// const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  darkMode: false,
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  prefix: '',
  important: true,
  separator: ':',
  theme: {
    customForms: (theme) => ({
      default: {
        select: {
          '&:focus': {
            outline: 'none',
            'box-shadow': 'none',
            borderColor: theme('colors.primary.300'),
          },
        },
        input: {
          '&:focus': {
            outline: 'none',
            'box-shadow': 'none',
            borderColor: theme('colors.primary.300'),
          },
        },
        checkbox: {
          '&:focus': {
            outline: 'none',
            'box-shadow': 'none',
            borderColor: theme('colors.primary.300'),
          },
        },
      },
    }),
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
    },
    extend: {
      colors: {
        primary: {
          100: '#fda8a8',
          200: '#fe5454',
          300: '#ff0000',
          400: '#bf0000',
          500: '#7f0000',
        },
        black: '#000000',
        white: '#ffffff',
        secondary: {
          100: '#f7f7f7',
          200: '#cccccc',
          300: '#a2a2a2',
          400: '#787878',
          500: '#4d4d4d',
        },
        success: '#3cd097',
      },
    },
  },
  variants: {
    margin: ['responsive', 'hover', 'first'],
    borderColor: ['group-hover', 'hover'],
    boxShadow: ['group-hover', 'hover'],
    backgroundColor: ['group-hover', 'hover'],
    textColor: ['responsive', 'focus', 'hover', 'group-hover', 'first'],
  },
  plugins: [
    require('@tailwindcss/custom-forms'),
    require('@tailwindcss/typography'),
  ],
}
