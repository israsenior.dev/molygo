const polka = require('polka')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
const app = polka()

const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

async function start() {
  const nuxt = new Nuxt(config)
  const { host, port } = nuxt.options.server
  await nuxt.ready()

  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  app.use((req, res, next) => {
    res.setHeader('X-Powered-By', 'Bloggen-CMS')
    res.setHeader(
      'Cache-Control',
      'public, must-revalidate, max-age=300,s-maxage=10,stale-while-revalidate=43200, stale-if-error=86400'
    )
    next()
  })

  app.use(nuxt.render)

  app.listen(port, host)

  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true,
  })
}

start()
