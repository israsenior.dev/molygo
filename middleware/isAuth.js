export default function ({ app, redirect, route, error }) {
  const hasToken = !!app.$apolloHelpers.getToken()
  if (!hasToken) {
    error({ errorCode: 403, message: 'You are not allowed to see this' })
    // console.log(route)
    redirect({
      name: 'signin',
      params: { redirectTo: route.path },
    })
  }
}
