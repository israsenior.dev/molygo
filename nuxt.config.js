export default {
  // server: {
  //   port: 3000,
  //   host: '0.0.0.0',
  //   timing: false,
  // },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'Mōlygo',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/scss/tailwind.scss', 'boxicons/css/boxicons.min.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '~/plugins/vue-lazyload.js', mode: 'client' },
    { src: '~/plugins/vue-carousel.js', mode: 'client' },
    // https://github.com/khoanguyen96/vue-paypal-checkout#usage-with-nuxt-js---npm
    { src: '~/plugins/paypal.js', ssr: false },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: false,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://github.com/nuxt-community/apollo-module
    '@nuxtjs/apollo',
    'nuxt-purgecss',
  ],

  // https://tailwindcss.nuxtjs.org/options
  tailwindcss: {
    cssPath: '~/assets/scss/tailwind.scss',
    configPath: '~/tailwind.config.js',
    exposeConfig: false,
    viewer: false,
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // https://github.com/nuxt-community/apollo-module#advanced-configuration
  apollo: {
    clientConfigs: {
      default: '~/plugins/apollo-client-config.js',
    },
  },

  /*
   ** Stylelint
   ** https://github.com/nuxt-community/stylelint-module#options
   */
  stylelint: {
    extends: ['stylelint-config-standard'],
    rules: {
      'at-rule-no-unknown': [
        true,
        {
          ignoreAtRules: [
            'tailwind',
            'apply',
            'variants',
            'responsive',
            'screen',
          ],
        },
      ],
      'declaration-block-trailing-semicolon': null,
      'no-descending-specificity': null,
    },
    /*
     ** Tailwind
     ** https://tailwindcss.com/docs/fill
     */
    tailwindcss: {
      // cssPath: '~/assets/css/tailwind.css',
      configPath: 'tailwind.config.js',
      exposeConfig: false,
      // config: {}
    },
    /*
     ** Axios module configuration
     ** See https://axios.nuxtjs.org/options
     */
    axios: {},
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    extractCSS: true,
    postcss: {
      plugins: { tailwindcss: './tailwind.config.js' },
    },
  },
  purgeCSS: { mode: 'postcss' },
}
