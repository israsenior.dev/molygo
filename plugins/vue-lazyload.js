import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error:
    'https://admin.molygo.com/wp-content/uploads/woocommerce-placeholder.png',
  loading: 'https://admin.molygo.com/wp-content/uploads/loader.gif',
  attempt: 1,
})
