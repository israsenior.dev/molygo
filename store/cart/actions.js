import _filter from 'lodash/filter'
import fillCart from '~/apollo/mutations/fillCart.gql'
import emptyCart from '~/apollo/mutations/emptyCart.gql'

export default {
  setCart({ commit, state }, payload) {
    const match = _filter(state.items, function (o, index) {
      return o.productId === payload.item.productId &&
        o.variationId === payload.item.variationId
        ? ((payload.index = index), true)
        : false
    })

    match.length > 0 ? commit('ADD_ITEM', payload) : commit('SET_CART', payload)
  },

  async emptyCart({ commit, state }) {
    try {
      await this.app.apolloProvider.clients.defaultClient
        .mutate({
          mutation: emptyCart,
          variables: {
            clientMutationId:
              this.app.store.state.auth.clientMutationId || null,
          },
          update: (data) => {},
        })
        .then((data) => {
          commit('EMPTY_CART')
        })
    } catch (error) {
      console.error(error)
      // commit('SET_ERROR', {
      //   message: 'There was an error. Please try again.',
      //   type: 'FAILED_ADD_TO_CART',
      // })
    }
  },

  async fillCart({ commit, state }, payload) {
    // console.log('action ', payload)
    const log = JSON.parse(localStorage.getItem('log'))

    if (log) {
      // console.log(localStorage.getItem('authToken'))
      this.$apolloHelpers.onLogout()
      // this.$apolloHelpers.onLogin(localStorage.getItem('authToken'))
      // console.log('redirectTo fillCart', payload.redirectTo)
      this.app.store.dispatch('auth/logIn', {
        username: log.username,
        password: log.password,
        clientMutationId: Math.random().toString(36).substring(2),
        redirectTo: payload.redirectTo || '/',
      })
    }

    try {
      await this.app.apolloProvider.clients.defaultClient
        .mutate({
          mutation: fillCart,
          variables: {
            input: {
              items: payload.items,
              coupons: payload.coupons,
            },
          },
          update: (data) => {},
        })
        .then((data) => {
          // console.log(data, 'then')
          commit('FILL_CART', data.data.fillCart.cart)

          if (data.extensions.debug.length > 0) {
            // console.log('error')
            this.app.store.dispatch('system/setNotification', {
              type: 'error',
              title: 'There was an error!',
              message: data.extensions.debug[0].message[0].reasons[0],
              duration: 5000,
            })
          } else {
            this.app.store.dispatch('system/setNotification', {
              type: 'check',
              title: 'Added!',
              message: 'The product was successfully added.',
              duration: 5000,
            })
          }
        })
    } catch (error) {
      console.error(error)
      this.app.store.dispatch('system/setNotification', {
        type: 'error',
        title: 'Error!',
        message: 'There was an error. Please try again.',
        duration: 5000,
      })
    }
  },

  updateCart({ commit, state }, payload) {
    commit('ADD_ITEM', payload)
    commit('SUBSTRACT_ITEM', payload)
  },

  removeItem({ commit, state }, index) {
    state.items.length > 1 ? commit('REMOVE_ITEM', index) : commit('EMPTY_CART')
  },

  setCheckout({ commit, state }, payload) {
    commit('SET_CHECKOUT', payload)
  },

  removeAllItemsFromSystem({ commit, state }) {
    commit('REMOVE_ITEMS_FROM_SYSTEM')
  },

  setError({ commit, state }, payload) {
    // commit('SET_ERROR', payload)
    this.app.store.dispatch('system/setNotification', {
      type: 'error',
      title: 'There was an error!',
      message: payload.message,
      duration: 10000,
    })
  },
}
