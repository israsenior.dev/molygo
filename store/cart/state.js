export default () => ({
  items: [],
  coupons: [],
  cart: null,
  order: null,
  checkout: {
    authorized: null,
    completed: null,
    cancelled: null,
  },
  errors: null,
})
