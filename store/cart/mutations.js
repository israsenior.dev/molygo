export default {
  SET_CART(state, payload) {
    state.items.push(payload.item)
    state.coupons = payload.coupons

    this.dispatch('cart/fillCart', {
      items: state.items,
      coupons: state.coupons,
      redirectTo: payload.redirectTo,
    })
  },

  ADD_ITEM(state, payload) {
    state.items[payload.index].quantity =
      state.items[payload.index].quantity + payload.item.quantity

    this.dispatch('cart/fillCart', {
      items: state.items,
      coupons: state.coupons,
      redirectTo: payload.redirectTo,
    })
  },

  REMOVE_ITEM(state, index) {
    state.items.splice(index, 1)

    this.dispatch('cart/fillCart', {
      items: state.items,
      coupons: state.coupons,
      redirectTo: '/cart',
    })
  },

  EMPTY_CART(state) {
    state.items = []
    state.cart = null
  },

  REMOVE_ITEMS_FROM_SYSTEM(state) {
    state.items = []
  },

  FILL_CART(state, payload) {
    // console.log('mutation ', payload)
    state.cart = payload
  },

  SET_CHECKOUT(state, payload) {
    state.checkout[payload.event] = payload.data
  },

  SET_ERROR(state, payload) {
    state.errors = payload
    setTimeout(() => {
      state.errors = null
    }, 4000)
  },
}
