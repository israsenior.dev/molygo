import logIn from '~/apollo/mutations/user/logIn.gql'
import updateCustomer from '~/apollo/mutations/user/updateCustomer.gql'

export default {
  setCustomer({ commit, state }, payload) {
    // commit('SET_CUSTOMER', payload)
    this.$apolloHelpers.onLogin(this.app.store.state.auth.customer.jwtAuthToken)
    this.$router.push(payload.redirectTo || '/')
  },

  async logIn({ commit, state }, payload) {
    const log = JSON.parse(localStorage.getItem('log'))

    const input = {
      username: log ? log.username : payload.username,
      password: log ? log.password : payload.password,
      clientMutationId: log
        ? log.clientMutationId
        : Math.random().toString(36).substring(2),
    }

    try {
      await this.app.apolloProvider.clients.defaultClient
        .mutate({
          mutation: logIn,
          variables: {
            input,
          },
          update: (data) => {},
        })
        .then((data) => {
          commit('SET_CUSTOMER', {
            customer: data.data.login.customer,
            user: data.data.login.user,
            clientMutationId: data.data.login.clientMutationId,
          })

          this.$apolloHelpers.onLogin(
            data.data.login.customer.jwtAuthToken,
            undefined,
            { expires: 7 }
          )
          localStorage.setItem(
            'authToken',
            data.data.login.customer.jwtAuthToken
          )

          localStorage.setItem('log', JSON.stringify(input))

          // console.log('redirectTo login', payload)
          this.$router.push(payload.redirectTo || '/')
        })
    } catch (error) {
      console.error(error)
    }
  },

  async logOut({ commit, state }) {
    await this.$apolloHelpers.onLogout()
    commit('LOGOUT')
    localStorage.removeItem('authToken')
    this.$router.push('/')
  },

  updateCustomer({ commit, state }, payload) {
    // const log = JSON.parse(localStorage.getItem('log'))

    // if (log) {
    //   this.$apolloHelpers.onLogout()
    //   this.app.store.dispatch('auth/logIn', {
    //     redirectTo: payload.redirectTo || '/',
    //   })
    // }
    this.app.apolloProvider.clients.defaultClient
      .mutate({
        mutation: updateCustomer,
        variables: {
          input: {
            id: this.app.store.state.auth.user.id,
            clientMutationId: this.app.store.state.auth.clientMutationId,
            billing: payload.billing,
            shipping: payload.sameForShipping
              ? payload.billing
              : payload.shipping,
            shippingSameAsBilling: payload.sameForShipping,
          },
        },
        update: (data) => {},
      })
      .then((data) => {
        commit('SET_CUSTOMER', {
          customer: data.data.updateCustomer.customer,
          user: this.app.store.state.auth.user,
          clientMutationId: data.data.updateCustomer.clientMutationId,
        })

        this.app.store.dispatch('system/setNotification', {
          type: 'check',
          title: 'Successfully saved!',
          message: 'The profile information was uploaded.',
          duration: 5000,
        })

        this.$router.push(payload.redirectTo || '/profile')
      })
      .catch((error) => {
        console.error(error)
        this.app.store.dispatch('system/setNotification', {
          type: 'error',
          title: 'Error while saving',
          message: 'Something has gone wrong. Please try again later.',
          duration: 5000,
        })
        this.app.store.dispatch('auth/logOut')
        this.app.store.dispatch('auth/logIn')
        this.$router.push('/')
      })
  },
}
