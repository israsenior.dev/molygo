export default {
  SET_CUSTOMER(state, payload) {
    state.customer = payload.customer
    state.user = payload.user
    state.clientMutationId = payload.clientMutationId
  },

  LOGOUT(state) {
    state.customer = null
  },
}
