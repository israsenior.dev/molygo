export default {
  setNotification({ commit, state }, payload) {
    commit('SET_NOTIFICATION', payload)

    setTimeout(() => {
      commit('UNSET_NOTIFICATION')
    }, payload.duration)
  },

  unsetNotification({ commit, state }) {
    commit('UNSET_NOTIFICATION')
  },
}
