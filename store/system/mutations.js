export default {
  SET_NOTIFICATION(state, payload) {
    state.notification = payload
  },

  UNSET_NOTIFICATION(state) {
    state.notification = null
  },
}
